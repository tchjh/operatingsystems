#ifndef HELPER_H
#define HELPER_H


typedef struct fun_desc 
{
/* 
   struct to hold function's name and pointer to implementation.
   an array of this type should be constructed in order to properly
   manage 'menu' construct.
*/
    char *name;
    void (*fun)(void);
} fun_desc;

int slen(const char * str);
char * _itoa(int num);

#endif
