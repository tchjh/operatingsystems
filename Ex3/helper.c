
/* return the length of a string */
int slen(const char * str)
{
	int i = -1;
	while (str[++i]);

	return i;
}

#define BUFFER_SIZE 13

char buffer[BUFFER_SIZE];

/* returns the string representation of an integer */
char * _itoa(int num)
{
	char* p = buffer+BUFFER_SIZE-1;
	int neg = num<0;
	
	if(neg)
		num = -num;
	
	*p='\0';
	do 
		*(--p) = '0' + num%10; 
	while(num/=10);
	
	if(neg) 
		*(--p) = '-';
	
	return p;
}
