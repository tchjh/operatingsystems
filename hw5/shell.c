#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "LineParser.h"
void execute(cmdLine *pCmdLine);
void changeCurrentDir(char* dirr);
void myEcho(cmdLine *pCmdLine);
int main()
{
    char dirName[1024];
    char cmdLineStringToParse[2048];
    cmdLine* parsedCmdLine = NULL;
    while(1)
    {
        getcwd(dirName, 1024);
        printf("%s", dirName);
        fgets(cmdLineStringToParse, 2048, stdin);
        parsedCmdLine = parseCmdLines(cmdLineStringToParse);
        if(strcmp(parsedCmdLine->arguments[0],"quit") == 0)
        {
            freeCmdLines(parsedCmdLine);
            exit(0);
        }
        else if(strcmp(parsedCmdLine->arguments[0],"cd") == 0)
        {
            changeCurrentDir(parsedCmdLine->arguments[1]);
        }
        else if(strcmp(parsedCmdLine->arguments[0],"myecho") == 0)
        {
            myEcho(parsedCmdLine);
        }
        else
        {
            execute(parsedCmdLine);
        }
    }
}
void execute(cmdLine *pCmdLine)
{
    pid_t pid = fork();
    int child_status,input,output;
    if(pid == 0)
    {
        if(pCmdLine->inputRedirect != NULL)
        {
            input = open(pCmdLine->inputRedirect, O_RDONLY, 0);
            close(0);
            dup(input);
            close(input);
        }
        if(pCmdLine->outputRedirect != NULL)
        {
            output = open(pCmdLine->outputRedirect, O_WRONLY, 0);
            close(1);
            dup(output);
            close(output);
        }
        if(execvp(pCmdLine->arguments[0], pCmdLine->arguments) < 0)
        {
            perror("error");
            exit(0);
        }
        close(input);
        close(output);
        exit(0);
    }
    else
    {
        waitpid(pid,&child_status,0);
    }
}
void changeCurrentDir(char* dir)
{
    int isOk = chdir(dir);
    if(isOk != 0)
    {
        printf("Error cd\n");
    }
}
void myEcho(cmdLine *pCmdLine)
{
    int i;
    for(i=1;i<pCmdLine->argCount;i++)
    {
        printf("%s \n",pCmdLine->arguments[i]);
    }
}