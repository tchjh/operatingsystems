#include <unistd.h>
#include <stdio.h>
#include <string.h>
int main()
{
	char magshimim[50];
	int pid;
	int filedes[2];
	if(pipe(filedes) == -1)
	{
		perror("Pipe was failed with:");
	}
	else
	{
		pid = fork();
		if(pid == -1)
		{
			perror("Fork was failed with");
		}
		else if(pid > 0)
		{
			write(filedes[1], "magshimim", 50);
		}
		else
		{
			read(filedes[0], magshimim, 50);
			puts(magshimim);
		}
		close(filedes[0]);
		close(filedes[1]);
	}
	return 0;
}
