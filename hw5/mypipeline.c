#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
int main()
{
    int fd[2],child1,child2;
    char* args1[] = { "ls", "-l", NULL };
    char* args2[] = { "tail",  "-n", "2", NULL};
    if(pipe(fd) == -1)
    {
        perror("Pipe failed with:");
    }
    else
    {
        child1 = fork();
        if(child1 == -1)
        {
            perror("Fork failed with:");
            exit(0);
        }
        if(child1 == 0)
        {
        close(1); 
        dup(fd[1]);
        close(fd[0]);
        execvp(args1[0], args1);
        exit(0);
        }
            close(fd[1]);
            child2 = fork();
            if(child2 == -1)
            {
                perror("Fork failed with:");
                exit(0);
            }
            if(child2 == 0)
            {
                close(0);
                dup(fd[0]); 
                close(fd[1]); 
                execvp(args2[0], args2);
                exit(0);
            }
            else
            {
                close(fd[0]);
                waitpid(-1, &child1, 0);
                waitpid(-1, &child2, 0);
            }
    }
    return(0);
}