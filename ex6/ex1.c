#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <linux/limits.h>
#include <fcntl.h>
#include <signal.h>
int main()
{
	pid_t child = fork();
	if(child == 0)
	{
	    while(1)
	    {
	    	printf("I'm still alive\n");
	    }
	}
	else
	{
		sleep(1);
		printf("Dispatching\n");
		kill(child,SIGQUIT);
		printf("Dispatched\n");
	}
}