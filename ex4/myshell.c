#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "LineParser.h"
#define PATH_MAX 100
#define MAX_COMMAND_SIZE 2048
void execute(cmdLine *pCmdLine,char* argvv);
int main(int argc, char *argv[])
{	
	cmdLine* line = NULL;
	char currnetPath[PATH_MAX];
	char command[PATH_MAX];
	FILE * f = fopen ("actions.txt", "r");
	int i = 0;
	if(f)
	{
		while(1)
		{
				getcwd(currnetPath, PATH_MAX);
				puts(currnetPath);
				fgets(command,MAX_COMMAND_SIZE, f);
				line = parseCmdLines(command);
				if(strcmp("quit",line->arguments[i]) == 0)
				{
					return 0;
				}
				else
				{
					execute(line,argv[0]);
				}
				i++;
		}
	}
	return 0;
}
void execute(cmdLine *pCmdLine,char* argvv)
{
	if((execvp(argvv, pCmdLine->arguments)<0))
	{
		perror("The error is:");
	}
}