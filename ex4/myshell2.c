#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "LineParser.h"
#define PATH_MAX 100
#define MAX_COMMAND_SIZE 2048
void execute(cmdLine *pCmdLine);
int main(int argc, char *argv[])
{	
	cmdLine* line = NULL;
	char currnetPath[PATH_MAX];
	char command[PATH_MAX];
	FILE * f = fopen ("actions.txt", "r");
	if(f)
	{
			getcwd(currnetPath, PATH_MAX);
	 		puts(currnetPath);
			fgets(command,MAX_COMMAND_SIZE, f);
			line = parseCmdLines(command);
			execute(line);
	}
	return 0;
}
void execute(cmdLine *pCmdLine)
{
    pid_t pid = fork();
   	int child_status;
	if(pid == 0)
	{
	    if(execvp(pCmdLine->arguments[0], pCmdLine->arguments) < 0)
	    {
	      	perror("error");
	     	exit(0);
	    }
	    exit(0);
	}
	else
	{
		waitpid(pid,&child_status,0);
	}
}