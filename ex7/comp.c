#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#define SIZE 1024
void func(int signum);
int main()
{
	while(access("/tmp/GO", F_OK));
	FILE *f = popen("ps", "r");
	char buf[SIZE];
	char temp[5];
	int pid;
	signal(SIGSTOP,func);
	signal(SIGQUIT,func);
	signal(SIGKILL,func);
	signal(SIGTERM,func);
	fgets(buf, 1024, f);
	while(fgets(buf, 1024, f) != NULL)
	{
		temp[0] = buf[1];
		temp[1] = buf[2];
		temp[2] = buf[3];
		temp[3] = buf[4];
		temp[4] = 0;
		pid =atoi(temp);
		printf("pid killed = %d",pid);
		kill(pid,SIGQUIT);
	}
    return 0;
}
void func(int signum)
{
}
