#include <unistd.h>
typedef struct metadata_block * p_block;
struct metadata_block 
{
	size_t size;
	p_block* next;
	int isFreed;
};
p_block *memoryArray = NULL;
void * malloc(size_t size);
p_block * findFreeBlock(p_block **last, size_t size);
p_block* memoryAdd(p_block* last, size_t size);
int main()
{
	char* str = (char*)malloc(5);
	str="aaaa";
	write(0,str,5);
}
void * malloc(size_t size)
{
	p_block* temp = NULL;
	if(memoryArray == NULL)
	{
		temp = 	memoryAdd(memoryArray,size);
	}
	else
	{
		temp = findFreeBlock(&memoryArray,size);
		if(temp == NULL)
		{
			temp = 	memoryAdd(memoryArray,size);
		}
	}
	return temp;
}
p_block * findFreeBlock(p_block **last, size_t size) 
{
  p_block *currNode = memoryArray;
  while (currNode) 
  {
  	if(currNode->isFreed == 1)
  	{
  		if(currNode->size >= size)
  		{
		    *last = currNode;
		    currNode = currNode->next;
		}
	}
  }
  return currNode;
}
p_block* memoryAdd(p_block* last, size_t size) 
{
  p_block *block;
  block = sbrk(0);
  void *request = sbrk(size + sizeof(p_block));
  if (request == (void*)-1) 
  {
    return NULL;
  }
  if (last) 
  {
    last->next = block;
  }
  block->size = size;
  block->next = NULL;
  block->isFreed = 0;
  return block;
}
