#include <unistd.h>
typedef struct metadata_block * p_block;
struct metadata_block 
{
	size_t size;
	p_block next;
	int free;
};
p_block memoryArray = NULL;
void * malloc(size_t size);
p_block findFreeBlock(p_block *last, size_t size);
p_block memoryAdd(p_block last, size_t size);
void free(void *ptr);
void *realloc(void *ptr, size_t size);
p_block get_block_ptr(void *ptr);
int main()
{
	char* str = (char*)malloc(5);
	str="aaaa";
	str = realloc(str,13);
	str="aaaaaaaaaaaa";
	write(0,str,13);
}
void * malloc(size_t size)
{
	p_block temp = NULL;
	if(memoryArray == NULL)
	{
		temp = 	memoryAdd(memoryArray,size);
	}
	else
	{
		temp = findFreeBlock(&memoryArray,size);
		if(temp == NULL)
		{
			temp = 	memoryAdd(memoryArray,size);
		}
	}
	return temp;
}
p_block findFreeBlock(p_block *last, size_t size) 
{
  p_block currNode = memoryArray;
  while (currNode) 
  {
  	if(currNode->free == 1)
  	{
  		if(currNode->size >= size)
  		{
		    *last = currNode;
		    currNode = currNode->next;
		}
	}
  }
  return currNode;
}
p_block memoryAdd(p_block last, size_t size) 
{
  p_block block;
  block = sbrk(0);
  void *request = sbrk(size + sizeof(struct metadata_block));
  if (request == (void*)-1) 
  {
    return NULL;
  }
  if (last) 
  {
    last->next = block;
  }
  block->size = size;
  block->next = NULL;
  block->free = 0;
  return block;
}
p_block get_block_ptr(void *ptr) 
{
  return (p_block)ptr - 1;
}
void free(void *ptr) {
	if (ptr == NULL) 
	{
	}
	else
	{
		p_block memBlock = get_block_ptr(ptr);
		memBlock->free = 1;
	}
}
void *realloc(void *ptr, size_t size) 
{
	if (ptr == NULL)
	{
	}
	else
	{
	p_block block_ptr = get_block_ptr(ptr);
	void *new_ptr;
	new_ptr = malloc(size);
	if (!new_ptr) 
	{
	  return NULL;
	}
	return new_ptr;
	}
}