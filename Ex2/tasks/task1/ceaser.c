#include "ceaser.h"
#include "string.h"
char shift_letter(char letter, int offset)
{
	'a' + (letter - 'a' + offset) % 26;
	return ('a' + (letter - 'a' + offset) % 26);
}

char * shift_string(char * input, int offset)
{
	int length = strlen(input);
	int i;
	input[length] = 0;
	for (i = 0; i < length; ++i)
		input[i] = shift_letter(input[i],offset);

	return input;
}
