#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int countlines(FILE* f);
int main(int argc, char ** argv)
{
	srand(time(0));
	int linesNumber = 0,lineToPrint = 0;
	char command[100];
	FILE * f = fopen (argv[1], "r");
	if(f)
	{
		linesNumber = countlines(f);
		lineToPrint = (rand() % linesNumber) + 1;
		sprintf (command, "head -n %d %s | tail -1",lineToPrint , argv[1]);
		system(command);
		close(f);
	}
	else
	{
		printf("Error,file doesnt exist!");
	}
	return 0;
}
int countlines(FILE* f)
{                                  
  int ch=0;
  int lines=0;
  if (f == NULL)
  {
  	return 0;
  }
  while ((ch = fgetc(f)) != EOF)
    {
      if (ch == '\n')
    lines++;
    }
  return lines;
}
